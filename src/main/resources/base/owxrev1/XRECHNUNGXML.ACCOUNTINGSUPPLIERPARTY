..!interpreter english translate noabbrev
..*****************************************************************************
..  FOP-Name       : XRECHNUNGXML.ACCOUNTINGSUPPLIERPARTY
..  Author         : Chinthaka
..  Functionality  : XML data generator for the Infosystem ev/EVVORGANG to build
..                   invoice Accounting Supplier Party
.. Date            : 18-Nov-2020
..*****************************************************************************
..
.type text xPartyId                  ? _F|defined(U|xPartyId)
.type text xPartyName                ? _F|defined(U|xPartyName)
.type text xStreetName               ? _F|defined(U|xStreetName)
.type text xAdditionalStreetName     ? _F|defined(U|xAdditionalStreetName)
.type text xCityName                 ? _F|defined(U|xCityName)
.type text xPostalZone               ? _F|defined(U|xPostalZone)
.type text xCountrySubentity         ? _F|defined(U|xCountrySubentity)
.type text xIdentificationCode       ? _F|defined(U|xIdentificationCode)
.type text xPartyTaxSchemeCompanyID  ? _F|defined(U|xPartyTaxSchemeCompanyID)
.type text xTaxSchemeId              ? _F|defined(U|xTaxSchemeId)
.type text xPartyLegalEntityRegName  ? _F|defined(U|xPartyLegalEntityRegName)
.type text xContactName              ? _F|defined(U|xContactName)
.type text xContactTelephone         ? _F|defined(U|xContactTelephone)
.type text xContactElectronicMail    ? _F|defined(U|xContactElectronicMail)
..
.set trans -
..
.formula U|xPartyId                 = 'M|vorgang^kl2^frbez'
.formula U|xPartyName               = 'F|sysinfo(10)^ans' << ";"
.formula U|xStreetName              = 'F|sysinfo(10)^str' << ";"
.formula U|xAdditionalStreetName    = 'F|sysinfo(10)^str' >> ";" << ";"
.formula U|xCityName                = 'F|sysinfo(10)^nort'
.formula U|xPostalZone              = 'F|sysinfo(10)^plz'
.formula U|xCountrySubentity        = 'F|sysinfo(10)^region^rnamebspr'
.formula U|xIdentificationCode      = 'F|sysinfo(10)^staat^iso2'
.formula U|xPartyTaxSchemeCompanyID = 'F|sysinfo(10)^ustid'
.formula U|xTaxSchemeId             = "VAT"
.formula U|xPartyLegalEntityRegName = 'F|sysinfo(10)^ans'
.formula U|xContactName             = 'M|vorgang^betreuer^ans'
.formula U|xContactTelephone        = 'M|vorgang^betreuer^tele'
.formula U|xContactElectronicMail   = 'M|vorgang^betreuer^email'
..
..*****************************************************************************
!OUTPUT
..*****************************************************************************
..
   <cac:AccountingSupplierParty>
      <cac:Party>
         <cac:PartyIdentification>
            <cbc:ID>'U|xPartyId'</cbc:ID>
         </cac:PartyIdentification>
         <cac:PartyName>
            <cbc:Name>'U|xPartyName'</cbc:Name>
         </cac:PartyName>
         <cac:PostalAddress>
            <cbc:StreetName>'U|xStreetName'</cbc:StreetName>
            <cbc:AdditionalStreetName>'U|xAdditionalStreetName'</cbc:AdditionalStreetName>
            <cbc:CityName>'U|xCityName'</cbc:CityName>
            <cbc:PostalZone>'U|xPostalZone'</cbc:PostalZone>
            <cbc:CountrySubentity>'U|xCountrySubentity'</cbc:CountrySubentity>
            <cac:Country>
               <cbc:IdentificationCode>'U|xIdentificationCode'</cbc:IdentificationCode>
            </cac:Country>
         </cac:PostalAddress>
         <cac:PartyTaxScheme>
            <cbc:CompanyID>'U|xPartyTaxSchemeCompanyID'</cbc:CompanyID>
            <cac:TaxScheme>
               <cbc:ID>'U|xTaxSchemeId'</cbc:ID>
            </cac:TaxScheme>
         </cac:PartyTaxScheme>
         <cac:PartyLegalEntity>
            <cbc:RegistrationName>'U|xPartyLegalEntityRegName'</cbc:RegistrationName>
         </cac:PartyLegalEntity>
         <cac:Contact>
            <cbc:Name>'U|xContactName'</cbc:Name>
            <cbc:Telephone>'U|xContactTelephone'</cbc:Telephone>
            <cbc:ElectronicMail>'U|xContactElectronicMail'</cbc:ElectronicMail>
         </cac:Contact>
      </cac:Party>
   </cac:AccountingSupplierParty>
..
.continue
